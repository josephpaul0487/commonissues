# FLUTTER
1. #####  IOS BUILD ERRORS

 	- **Flutter - iOS: Command /bin/sh failed with exit code 255**
		[stackoverflow](https://stackoverflow.com/questions/52577639/flutter-ios-command-bin-sh-failed-with-exit-code-255)
		flutter clean
		flutter build ios
	- **Xcode Command PhaseScriptExecution failed with a nonzero exit code**
		[stackoverflow](https://stackoverflow.com/questions/53289524/xcode-10-2-1-command-phasescriptexecution-failed-with-a-nonzero-exit-code)
		Keychain Access -> Right-click on login -> Lock & unlock again -> Clean Xcode project ->Make build again
		Xcode -> File -> Workspace Setting... -> change Build System to Legacy Build System
		
	- **@import module not found**

		change **Deployment Target version** to appropriate version   for jitsi_meet it is 11.0
        [jitsi_meet](https://github.com/gunschu/jitsi_meet/issues/16)

        *Check the **xcode version** is same as the libraries **compiled version**.*
        
        - **error: include of non-modular header inside framework module**
        
            Manually in xcode update the [Module].h to be in the flutter_freshchat target and public.
            
            eg for freshchat
                click FreshchatSDK.h  file (left side)
                click show file inspector icon on right side
                Under Target Member Ship select the module and set it as public

    - **Theme.of(key.currentContext, shadowThemeOnly: true) Not Found**
    	Search in .pub-cache/hosted/pub.dartlang.org/get-3.15.0/lib/get_navigation/src/extension_navigation.dart for

		Theme.of(key.currentContext, shadowThemeOnly: true)
		As replace it with

		Theme.of(key.currentContext)
		[stackoverflow](https://stackoverflow.com/questions/64706160/flutter-not-able-to-run-get-navigation-theme-error)

	- **SDK location not found. Define location with sdk.dir in the local.properties file or with an ANDROID_HOME environment variable**
		Add sdk.dir = /Users/USERNAME/Library/Android/sdk in local.properties in the android folder
		copy the local.properties to root folder

2. ##### ANDROID BUILD ERRORS

	-	**SDK location not found. Define location with sdk.dir in the local.properties file or with an ANDROID_HOME environment variable.**
		[stackoverflow](https://stackoverflow.com/questions/27620262/sdk-location-not-found-define-location-with-sdk-dir-in-the-local-properties-fil)
		set sdk directory path in local.properties   eg, sdk.dir = /Users/USERNAME/Library/Android/sdk
		check the settings.gradle have **include ':app'** line 

		Add the below codes to settings.gradle if the above solution is not working

		```
		include ':app'

		def flutterProjectRoot = rootProject.projectDir.parentFile.toPath()

		def plugins = new Properties()
		def pluginsFile = new File(flutterProjectRoot.toFile(), '.flutter-plugins')
		if (pluginsFile.exists()) {
		    pluginsFile.withReader('UTF-8') { reader -> plugins.load(reader) }
		}

		plugins.each { name, path ->
		    def pluginDirectory = flutterProjectRoot.resolve(path).resolve('android').toFile()
		    include ":$name"
		    project(":$name").projectDir = pluginDirectory
		}
		```

	-	**unexpected element <queries> found in <manifest>**
		[stackoverflow](https://stackoverflow.com/questions/62969917/how-do-i-fix-unexpected-element-queries-found-in-manifest)	
		classpath 'com.android.tools.build:gradle:version' - change version to  > 3.6.3  (in android root build.gradle file)
		distributionUrl=https\://services.gradle.org/distributions/gradle-version-all.zip - change version to  > 5.6.4  (in android/gradle/wrapper/gradle-wrapper.properties file)
---   

# ANDROID

---

# SWIFT

---

# PHP

---

# NODE

---

# VUE_JS

---

# ANGULAR_JS

---

# UNITY

---

